import React from 'react';
import { Container } from '../../components';

export default function(props) {
    return (
        <Container>
            <h1>
                This is a Private Route
            </h1>
        </Container>
    )
}