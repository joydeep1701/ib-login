import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from '../../components';

export default function LandingPage(props) {
    return (
        <Container>
            <h1>InterviewBit Academy Landing Page</h1>
            <ul>
                <li>
                    <Link to="auth">Authentication</Link>
                </li>
                <li>
                    <Link to="private">Private Page</Link>
                </li>
            </ul>
            
        </Container>
    )
}