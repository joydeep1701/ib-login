import { AUTH_LOGIN_START } from './actionTypes';
import { authLoginSuccess, authLoginError } from './actions'
import { AuthenticateUser } from './utils';

export const AuthenticationMiddleware = ({ dispatch }) => next => (action) => {
    if (action.type === AUTH_LOGIN_START) {
        console.log("[AUTH] Starting Authentication");

        AuthenticateUser(action.payload)
            .then(data => {
                dispatch(authLoginSuccess(data));
            })
            .catch(err => {
                dispatch(authLoginError(err));
            })

    }
    next(action);
}