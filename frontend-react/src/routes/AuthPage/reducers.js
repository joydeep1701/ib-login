import {
    AUTH_LOGIN_ERROR,
    AUTH_LOGIN_START,
    AUTH_LOGIN_SUCCESS,
} from './actionTypes';

const initialState = {
    authenticated: false,
    isAuthenticating: false,
    user: null,
    errors: false,
    error: null,
};

export function authenticationReducer(state=initialState, action) {
    // At times we need to create temporary variables for reducer to work,
    // ESLint advices not to create variables inside a switch 
    if (action.type === AUTH_LOGIN_START) {
        return {
            ...state,
            isAuthenticating: true,
            authenticated: false,
            user: null,
        }
    }
    if (action.type === AUTH_LOGIN_SUCCESS) {
        const {
            userId,
            jwtToken,
        } = action.payload;
        return {
            ...state,
            isAuthenticating: false,
            authenticated: true,
            user: {
                userId,
                jwtToken
            }
        }
    }
    if (action.type === AUTH_LOGIN_ERROR) {
        const { error, errors } = action.payload;
        return {
            ...state,
            authenticated: false,
            isAuthenticating: false,
            error,
            errors,
        }
    }
    return state;
}
