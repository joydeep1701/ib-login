import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { LoginForm } from '../../containers';
import './authpage.scss';
import { authLoginStart } from './actions';

function AuthPage(props) {
    if (props.auth.authenticated) {
        return (
            <Redirect to="/private" />
        )
    }

    return (
        <div className="auth-page">
            <LoginForm
                callback={v => props.authLoginStart(v)}
                isAuthenticating={props.auth.isAuthenticating}
                hasError={props.auth.errors}
                errorMessage={props.auth.error}
            />
        </div>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        authLoginStart
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage)