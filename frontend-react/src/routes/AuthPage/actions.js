import {
    AUTH_LOGIN_ERROR,
    AUTH_LOGIN_START,
    AUTH_LOGIN_SUCCESS,
} from './actionTypes';

export function authLoginStart({ userId, password }) {
    return {
        type: AUTH_LOGIN_START,
        payload: {
            userId,
            password
        }
    }
}
export function authLoginSuccess(payload) {
    return {
        type: AUTH_LOGIN_SUCCESS,
        payload,
    }
}
export function authLoginError(payload) {
    return {
        type: AUTH_LOGIN_ERROR,
        payload,
    }
}