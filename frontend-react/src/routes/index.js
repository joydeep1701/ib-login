import LandingPage from './LandingPage'
import AuthPage from './AuthPage';
import PrivatePage from './PrivatePage';

export {
    LandingPage,
    AuthPage,
    PrivatePage
}