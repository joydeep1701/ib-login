import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { authenticationReducer } from './routes/AuthPage/reducers';

export default history => combineReducers({
  router: connectRouter(history),
  auth: authenticationReducer,
});