import React, { Component } from 'react';
import zxcvbn from 'zxcvbn';
import {
    Button,
    Container,
    Input
} from '../../components';
import './login-form.scss'

class LoginForm extends Component {
    state = {
        username: {
            type: 'text',
            value: '',
            label: 'Username'
        },
        password: {
            type: 'password',
            value: '',
            label: 'password',
            score: null,
        },
    }
    handleInputChange = async (e) => {
        const {
            target: {
                name,
                value
            }
        } = e;
    
        await this.setState({
          [name]: {
              ...this.state[name],
              value,
          }
        });
    }
    passwordStrength = async (e) => {
        if (e.target.value === '') {
            await this.setState({
                password: {
                    ...this.state.password,
                    score: null,
                }
            });
        } else {
            const pw = zxcvbn(e.target.value);

            await this.setState({
                password: {
                    ...this.state.password,
                    score: pw.score,
                }
            });
        }
    }
    togglePassword = async () =>  {
        await this.setState({
            password: {
                ...this.state.password,
                type: this.state.password.type === 'input'? 'password': 'input',
            }
        });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const userId = this.state.username.value;
        const password = this.state.password.value;

        if (!userId || !password) {
            return alert('UserID/ Password missing');
        }

        this.props.callback({
            userId,
            password,
        })
    }

    render() {
        const { isAuthenticating, hasError, errorMessage } = this.props;
        return (
            <form action="" onSubmit={this.handleFormSubmit} className='login-form'>
                <Container>
                    <h2>InterviewBit Login</h2>
                </Container>
                <Container>
                    <Input
                        label={this.state.username.label}
                        name='username'
                        type={this.state.username.type}
                        value={this.state.username.value}
                        onChange={e => {
                            this.handleInputChange(e);
                        }}
                    />
                </Container>
                <Container>
                    <label className="password">
                        <Input
                            label={this.state.password.label}
                            name='password'
                            type={this.state.password.type}
                            value={this.state.password.value}
                            onChange={async e => {
                                // React uses synthetic events so once the callback is invoked
                                // the properties are nullified
                                // e.persist() allows us to access event properties in async way
                                e.persist();
                                await this.handleInputChange(e);
                                await this.passwordStrength(e);
                            }}
                        />
                        <span className="password-show" onClick={this.togglePassword}>
                            {this.state.password.type === 'input'? 'Hide': 'Show' }
                        </span>
                        <span className="password-stregth" data-score={this.state.password.score} />
                    </label>
                </Container>
                <br />
                <Container>
                    <Button color="blue" disabled={isAuthenticating}>
                        {isAuthenticating? "Please Wait" : "Login" }
                    </Button>
                </Container>
                {hasError? 
                    <Container>
                        <p>{errorMessage}</p>
                    </Container>
                : null}


            </form>
        );
    }
}

export default LoginForm;