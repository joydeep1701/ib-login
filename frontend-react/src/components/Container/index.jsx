import React from 'react';
import './container.scss';

export default function Container({ children, ...rest }) {
    return (
        <div
            className="container"
            {...rest}
        >
            {children}
        </div>
    )
}