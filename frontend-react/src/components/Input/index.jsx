import React from 'react';
import './input.scss';

export default function Input({ children, label, ...rest }) {
    return (
        <div>
            {label?<label className="label">{label}</label>:null}
            <input
                className="input"
                {...rest}
            >
                {children}
            </input>
        </div>

    )
}