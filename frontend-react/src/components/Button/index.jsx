import React from 'react';
import './button.scss';

export default function Button({ children, color, ...rest }) {
    return (
        <button
            {...rest}
            className={`btn btn-${color}`}
        >
            {children}
        </button>
    )
}