import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history'
import configureStore from './configureStore.js';
import App from './App';
import * as serviceWorker from './serviceWorker';

const initialState = {};
const browserHistory = createBrowserHistory();
const store = configureStore(initialState, browserHistory);


ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <App />
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));

serviceWorker.unregister();

export {
    browserHistory
};