import {AuthenticationMiddleware} from './routes/AuthPage/middleware';

const middlewares = [
    AuthenticationMiddleware,
];

export default middlewares;