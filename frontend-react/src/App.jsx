import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { LandingPage, AuthPage, PrivatePage } from './routes';
import { PrivateRoute } from './hoc/PrivateRoute'

const App = (props) => {
  const { isAuthenticated } = props;
  return (
    <div>
      <Router basename='/react'>
        <Switch>
          <Route exact path='/' component={LandingPage} />
          <Route exact path='/auth' component={AuthPage} />
          <PrivateRoute exact path='/private' component={PrivatePage} isAuthenticated={isAuthenticated} />
        </Switch>
      </Router>
    </div>
  )
}
const mapStateToProps = state => {
  return {
      isAuthenticated: state.auth.authenticated,
  };
};

export default connect(mapStateToProps)(App)