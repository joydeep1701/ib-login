import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import api from './routes';

// Load environment variables
dotenv.config();

// Get environment variables
const { 
    PORT=3000,
    API_ROOT='/',
    MONGO_PATH,
} = process.env;

// Initialize the express app
const app = express();
// Parse incoming request bodies in a middleware
// Before a handler reaches
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Router
app.use(`${API_ROOT}`, api);


app.listen(PORT, () => {
    console.log(`[SERVER] Server started on http://0.0.0.0:${PORT}`)
})


export default app;