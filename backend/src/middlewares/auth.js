import jwt from 'jsonwebtoken';
import env from 'dotenv';
// Load env vars
env.config();

const { JWT_KEY: key } = process.env;

if (!key) {
    console.error("ERROR: Key is not specified");
    process.exit(1);
}

export const isAuthenticated = (req, res, next) => {
    const token = req.headers["authorization"];

    if (!token)
        return res.status(401).send("Access denied. No token provided.");


    try {
        const decoded = jwt.verify(token, key);
        req.user = decoded;
        next();
    } catch(err) {
        res.status(400).send("Authentication Error. Invalid Token.");
    }
    
}