import express from 'express';
import { isAuthenticated } from '../middlewares/auth';

const router = express.Router();

/**
 * This will make every route defined below a private route which requires 
 * a authentication token to be present.
 * 
 */
// router.use(isAuthenticated);

router.get('/', (req, res) => {
    res.send("OK");
});

router.post('/user/create', (req, res) => {
    // Create a new user


});
router.post('/user/validate', (req, res) => {
    // Validate existing user
    
});

router.get('/user/all', isAuthenticated, (req, res) => {
    // Returns all of the users

})
export default router;