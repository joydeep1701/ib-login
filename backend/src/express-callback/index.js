export const makeExpressCallback = (controller) => (req, res) => {
    const {
        body,
        query,
        params,
        method,
        path,
        headers
    } =  req;

    const httpRequest = {
        body,
        query,
        params,
        method,
        path,
        headers
    };

    controller(httpRequest)
        .then(httpResponse => {
            if (httpResponse.headers)
                res.set(httpResponse.headers);
            res.type('json');
            res.status(httpResponse.statusCode).send(httpResponse.body);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send({
                error: 'Internal Server Error',
            })
        })

}