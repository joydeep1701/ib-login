export default function makeUsersDB({ makeDB }) {
    return Object.freeze({
        findAllUsers,
        findByUserID,
        createUser,
    });

    async function findAllUsers() {
        const db = await makeDB();
    }

    async function findByUserID({ userId: _id }) {
        const db = await makeDB();
    }

    async function createUser({ userId: _id, ...userData }) {
        const db = await makeDB();

    }
}