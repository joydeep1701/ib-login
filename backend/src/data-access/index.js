import makeUserDb from './users-db';
import { MongoClient } from 'mongodb';
import dotenv from 'dotenv';

dotenv.config();

const { MONGO_URL } = process.env;
const { MONGO_DBNAME } = process.env;

const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });

export async function makeDB() {
    if (!client.isConnected)
        await client.connect();
    
    return client.db(MONGO_DBNAME);
}

export const usersDB = makeUserDb({ makeDB });
